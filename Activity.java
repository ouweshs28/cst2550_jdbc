import java.sql.*;

public class Activity{
    public static void main (String args[]){
        String dburl="jdbc:mysql://localhost/dreamhome";
        String user="root";
        String pwd="";

        try{
            Connection conn=DriverManager.getConnection(dburl, user, pwd);
            Statement stat=conn.createStatement();
            String query="SELECT * FROM Staff ORDER BY lname";
            ResultSet rs=stat.executeQuery(query);

            while(rs.next()){
                System.out.println(rs.getString("lname")+ '\t'+ rs.getString("fname"));
            }
        }catch(SQLException ex){
                System.out.println("Error"+ ex.getMessage());
            }

        }
    }

