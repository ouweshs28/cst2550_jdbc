import java.util.Scanner;
import java.sql.*;

public class ExamplePrepareStatement {
	public static void main(String args[]) {
		String dburl = "jdbc:mysql://localhost/example";
		String user = "root";
		String pwd = "";

		Scanner input = new Scanner(System.in);

		try (Connection conn = DriverManager.getConnection(dburl, user, pwd)) {

			System.out.print("Enter name: ");
			String aName = input.nextLine();

			System.out.print("Enter password: ");
			String aPass = input.nextLine();

			String query = " SELECT * FROM user " + 
						   " WHERE name = ? AND password = ?";
			PreparedStatement preparedStatement = conn.prepareStatement(query);
			preparedStatement.setString(1, aName);
			preparedStatement.setString(2, aPass);

			ResultSet rs=preparedStatement.executeQuery();

			boolean none = true;

			while (rs.next()) {
				System.out.println(rs.getString("id") + '\t' + rs.getString("name") + '\t' + rs.getString("password"));

				none = false;
			}

			if (none)
				System.out.println("Sorry the name and/or password is incorrect");

		} catch (SQLException ex) {
			System.out.println("SQL error: " + ex.getMessage());
		}
	}
}
