DROP DATABASE mydb;
CREATE DATABASE mydb;
USE mydb;

CREATE TABLE User(
    id int(5) NOT NULL,
    name varchar (50) NOT NULL,
    password varchar (50) NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO User
VALUES(12345,"sam","pass"),
(12323,"jam","pass1"),
(12213,"kara","pass2");

