import java.sql.*;

public class ActivityOne{
    public static void main (String args[]){
        String dburl="jdbc:mysql://localhost/dreamhome";
        String user="root";
        String pwd="";

        try{
            Connection conn=DriverManager.getConnection(dburl, user, pwd);
            Statement stat=conn.createStatement();
            String query="SELECT lname,fname, city FROM Staff, Branch WHERE Staff.branchNo=Branch.branchNo ORDER BY lname";
            ResultSet rs=stat.executeQuery(query);

            while(rs.next()){
                String name =rs.getString("lname")+" "+rs.getString("fname");
                String city=rs.getString("city");
                System.out.println(name + " " + city +" staff");
            }
        }catch(SQLException ex){
                System.out.println("Error"+ ex.getMessage());
            }

        }
    }