CREATE DATABASE example;
USE example;

CREATE TABLE user (
  id int(11) NOT NULL,
  name varchar(50) NOT NULL,
  password varchar(50) NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO user
VALUES (1,'Bob','pass1'),
(2,'Sue','pasw0rd'),
(3,'Bill','aSecret'),
(4,'Jane','Jane1');
