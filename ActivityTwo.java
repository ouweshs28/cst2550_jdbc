import java.sql.*;

public class ActivityTwo{
    public static void main (String args[]){
        String dburl="jdbc:mysql://localhost/dreamhome";
        String user="root";
        String pwd="";

        try{
            Connection conn=DriverManager.getConnection(dburl, user, pwd);
            Statement stat=conn.createStatement();
            String query="SELECT COUNT(*) as totalStaff FROM Staff ";
            ResultSet rs=stat.executeQuery(query);

            while(rs.next()){
                System.out.println("Total Staffs in dreamhome "+rs.getString("totalStaff"));
            }
        }catch(SQLException ex){
                System.out.println("Error"+ ex.getMessage());
            }

        }
    }