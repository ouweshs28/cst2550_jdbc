import java.util.Scanner;
import java.sql.*;

public class VerySQLInjection{
    public static void main (String args[]){
        String dburl="jdbc:mysql://localhost/mydb";
        String user="ouweshs28";
        String pass="ouweshs28";

        Scanner scan = new Scanner(System.in);
        try{
            Connection connect= DriverManager.getConnection(dburl, user, pass);
            Statement stat = connect.createStatement();

            System.out.println("Enter name: ");
            String name= scan.nextLine();

            String password=" hacker ' or true";
            // other variations 
            //String password= " 1 ' or true or ' 1 ";


            String query="SELECT * FROM User" +
            " WHERE name = '" +name +"'" +
            " AND password = '" +password+";";


            ResultSet result = stat.executeQuery(query);
            boolean noResult= true;

            while (result.next()){
                System.out.println( result.getString("id") + '\t'+
                                    result.getString("name")+ '\t'+
                                    result.getString("password"));
                noResult=false;

            }
            if (noResult)
            System.out.println("Incorrect username or/and password");
        
        }catch(SQLException ex){
            System.out.println("SQL error"+ ex.getMessage());
        }

        scan.close();

        
    }
}