import java.util.Scanner;
import java.sql.*;

public class PreparedStatements{
    public static void main(String args[]){
        String dburl="jdbc:mysql://localhost/mydb";
        String user="ouweshs28";
        String pass="ouweshs28";

        Scanner input = new Scanner(System.in);
        try{
            Connection connect= DriverManager.getConnection(dburl, user, pass);

            System.out.println("Enter name: ");
            String name= input.nextLine();

            System.out.println("Enter password: ");
            String password= input.nextLine();

            String query="SELECT * FROM User"+
            " WHERE name = ? AND password = ?";


            PreparedStatement statement =connect.prepareStatement(query);
            statement.setString(1, name);
            statement.setString(2, password);

            

            ResultSet result= statement.executeQuery();


            boolean noResult= true;
            while (result.next()){
                System.out.println( result.getString("id") + '\t'+
                                    result.getString("name")+ '\t'+
                                    result.getString("password"));
                noResult=false;

            }
            if (noResult)
            System.out.println("Incorrect username or/and password");
        

        }catch(SQLException ex){
            System.out.println("SQL error"+ ex.getMessage());
        }
        input.close();

    }
}