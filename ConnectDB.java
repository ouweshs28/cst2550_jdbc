import java.sql.*;

public class ConnectDB {
    public static void main(String[] args) {
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/dreamhome", "ouweshs28", "ouweshs28");
            String sqlQuery = "SELECT Lname, Fname FROM Staff";
            Statement statement = conn.createStatement();
            ResultSet results = statement.executeQuery(sqlQuery);
            while (results.next())
                System.out.println(results.getString(1) + " " + results.getString(2));
        } catch (SQLException ex) {
            System.out.println("SQL error: " + ex.getMessage());
        }
    }
}
